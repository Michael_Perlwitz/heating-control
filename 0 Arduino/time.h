
#ifndef TIME_H
#define TIME_H


#include <Arduino.h>
#include "time.h"



class Time {

  private:

    unsigned long offset = 0;
    const byte offsetMezToUtc = 1;
    const byte daylightSavingTime = 0;

  public:

    void setSystemTime(unsigned long unixTime);
    unsigned long getUnixTime();
    float getHourOfDay();
    
};


#endif
