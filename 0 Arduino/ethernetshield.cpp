

#include <Ethernet2.h>
#include <SD.h>

#include "ethernetshield.h"


void EthernetShield::setup() {

  // disable w5100 while setting up SD
  pinMode(10, OUTPUT);
  digitalWrite(10, HIGH);
  SD.begin(4);

  Ethernet.begin(myMAC, myIP);

}

