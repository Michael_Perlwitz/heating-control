#ifndef TEMPSENSOR_H
#define TEMPSENSOR_H

#include <Arduino.h>
#include "tempsensor.h"


class TempSensor {

  private:
    const byte thermistorPin = A0;
    const byte VCC = 5; // 5V supply voltage
    const int rSerial = 10000; // 10kOhm resistor in series to thermistor
    const float kelvin = 273.15;
   
    float getUnweightedTemperature();



  public:
    TempSensor();

    float getTemperature();

};

#endif
