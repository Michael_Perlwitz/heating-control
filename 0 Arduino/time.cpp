
#include "time.h"



void Time::setSystemTime(unsigned long unixTime) {
  offset = unixTime - millis() / 1000;
}



unsigned long Time::getUnixTime() {
  return offset + millis() / 1000;
}



float Time::getHourOfDay() {
  
  unsigned long time = getUnixTime();

  long secondsOfDay = time % ((long) 24 * 60 * 60);
  float hourOfDay = secondsOfDay / ((float) 60 * 60);

  hourOfDay = hourOfDay + offsetMezToUtc + daylightSavingTime;

  if (hourOfDay > 24) {
    hourOfDay = hourOfDay - 24;
  }

  return hourOfDay;
}


