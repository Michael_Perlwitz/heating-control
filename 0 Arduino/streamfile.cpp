

#include "streamfile.h"

static const boolean DEBUG = false;




StreamFile::StreamFile() {

  if (DEBUG) Serial.println(F("StreamFile::StreamFile() called"));
  // initialize buffer levels
  inBufferLevel = 0;
  outBufferLevel = 0;

}



byte StreamFile::pullFromOutBuffer(char *c) {

  const boolean LOCAL_DEBUG = false;

  if (DEBUG) Serial.println(F("StreamFile::pullFromOutBuffer() called"));

  if (outBufferLevel >= 1) {

    // read first character
    char ch = outBuffer[0];

    // shift buffer by one position
    for (int i = 0; i <= outBufferLevel - 1; i++) {
      outBuffer[i] = outBuffer[i + 1];
    }

    // reduce buffer level by one
    outBufferLevel--;

    // refill outbuffer
    refillOutBuffer();

    // return read character
    *c = ch;
    if (LOCAL_DEBUG) {
      Serial.println(ch);
      Serial.println(*c);
    }
    return 1;

  } else {
    return 0;
  }

}



void StreamFile::refillOutBuffer() {

  const boolean LOCAL_DEBUG = false;

  if (DEBUG) Serial.println(F("StreamFile::refillOutBuffer() called"));

  // check if outbuffer is empty enough
  if (outBufferLevel <= maxOutBufferSize - maxReplacementStringSize) {

    // pull data from inbuffer
    char buf[maxOutBufferSize];
    byte bufSize;
    bufSize = pullWithReplacementsFromInbuffer(buf);

    if (LOCAL_DEBUG) {
      for (int i = 0; i <= bufSize - 1; i++) Serial.print(buf[i]);
      Serial.println();
    }
    // transfer to outbuffer
    for (int i = 0; i <= bufSize - 1; i++) {
      outBuffer[outBufferLevel + i] = buf[i];
    }

    // adjust level of outbuffer
    outBufferLevel += bufSize;
  }
  if (LOCAL_DEBUG) {
    for (int i = 0; i <= outBufferLevel - 1; i++) {
      Serial.print(outBuffer[i]);
    }
    Serial.println();
  }
}



byte StreamFile::pullWithReplacementsFromInbuffer(char *buf) {

  const boolean LOCAL_DEBUG_1 = false;
  const boolean LOCAL_DEBUG_2 = false;

  if (DEBUG || LOCAL_DEBUG_1 || LOCAL_DEBUG_2) Serial.println(F("StreamFile::pullWithReplacementsFromInbuffer() called"));

  byte matchID;

  // if inbuffer is filled up, check if it is simply to be read or a replacementstring has to be obtained (in case of a pattern match)
  if (inBufferLevel == maxInBufferSize) {
    matchID = patternMatch(inBuffer);
  } else {
    matchID = 0;
  }

  if (matchID == 0) {
    // obtain one character from the inbuffer
    char ch = ' ';
    byte numberOfChars = pullFromInbuffer(&ch); // number of chars is always one
    refillInBuffer();
    *buf = ch;
    if (LOCAL_DEBUG_1) {
      if (numberOfChars > 0) Serial.println(*buf);
      Serial.println(String(F("===Number of chars: ")) + String(numberOfChars));
    }
    return numberOfChars;
  } else {
    // obtain replacement string
    char replacementString[maxOutBufferSize];
    byte numberOfChars = getReplacementString(replacementString, matchID);
    flushInBuffer();
    refillInBuffer();
    strncpy(buf, replacementString, numberOfChars);
    if (LOCAL_DEBUG_2) {
      Serial.println(String(F("===pullWithReplacementsFromInbuffer")));
      Serial.print("matchID: "); Serial.println(matchID);
      Serial.print("replacementString: "); Serial.println(replacementString);
      Serial.print("numberOfChars: "); Serial.println(numberOfChars);
      for (int i = 0; i <= numberOfChars - 1; i++) Serial.print((replacementString)[i]);
      for (byte i = 0; i <= numberOfChars - 1; i++) Serial.print(*(buf + i));
      Serial.println(String(F("===Replacement string has characters: ")) + String(numberOfChars));
    }
    return numberOfChars;
  }

}

byte StreamFile::pullFromInbuffer(char *c) {

  const boolean LOCAL_DEBUG = false;

  if (DEBUG || LOCAL_DEBUG) Serial.println(F("StreamFile::pullFromInbuffer() called"));

  if (inBufferLevel >= 1) {

    // read first character
    char ch = inBuffer[0];

    // shift buffer by one position
    for (int i = 0; i <= inBufferLevel - 1; i++) {
      inBuffer[i] = inBuffer[i + 1];
    }

    // reduce buffer level by one
    inBufferLevel--;

    // refill inbuffer
    refillInBuffer();

    // return read character
    *c = ch;
    if (LOCAL_DEBUG) Serial.println(*c);
    return 1;

  } else {
    return 0;
  }

}



void StreamFile::flushInBuffer() {

  const boolean LOCAL_DEBUG = false;
  if (DEBUG || LOCAL_DEBUG) Serial.println(F("StreamFile::flushInBuffer() called"));
  inBufferLevel = 0;

}



void StreamFile::refillInBuffer() {

  const boolean LOCAL_DEBUG = false;

  if (DEBUG) Serial.println(F("StreamFile::refillInBuffer() called"));

  // check if there are empty place in inbuffer
  if (inBufferLevel <= maxInBufferSize - 1) {
    // pull for empty place the next characters from file, write to buffer and increase buffer level
    for (int i = inBufferLevel; i <= maxInBufferSize - 1; i++) {
      if (f.available()) {
        char c = f.read();
        inBuffer[i] = c;
        inBufferLevel++;

        if (LOCAL_DEBUG) {
          Serial.print(i);
          Serial.print(c);
          Serial.print(inBuffer[i]);
        }

      }
    }
    if (LOCAL_DEBUG) {
      for (int i = 0; i < inBufferLevel; i++) Serial.print(inBuffer[i]);
      Serial.println(String(F("===inBufferLevel: ")) + String(inBufferLevel));
    }
  }
}



byte StreamFile::patternMatch(char buf[maxInBufferSize]) {

  const boolean LOCAL_DEBUG = false;

  if (DEBUG) Serial.println(F("StreamFile::patternMatch() called"));

  byte returnValue = 0;

  for (byte i = 0; i <= maxReplacementItems-1; i++) {
    if (LOCAL_DEBUG) {
      Serial.print("buffer: "); Serial.println(buf);
      Serial.print("patternToBeReplaced: "); Serial.println(replItems.items[i].patternToBeReplaced);
      Serial.print("i: "); Serial.println(i);
    }

    if (charArrayCompare(buf, replItems.items[i].patternToBeReplaced)) {
      returnValue = i + 1;
      break;
    }
  }

  if (LOCAL_DEBUG) {
    Serial.print("matched pattern: "); Serial.println(returnValue);
  }
  return returnValue;
}



byte StreamFile::getReplacementString(char *replacementString, int matchID) {

  const boolean LOCAL_DEBUG = false;

  if (DEBUG) Serial.println(F("StreamFile::getReplacementString() called"));

  byte len = replItems.items[matchID - 1].length;

  for (int i = 0; i <= len - 1; i++) {
    *(replacementString + i) = replItems.items[matchID - 1].replacementString[i];
  }

  if (LOCAL_DEBUG) {
    //for (int i = 0; i <= len - 1; i++) Serial.print(str[i]); Serial.println();
    for (int i = 0; i <= len - 1; i++) Serial.print( *(replacementString + i) ); Serial.println();
  }

  return len;

}




boolean StreamFile::charArrayCompare(char c1[maxInBufferSize], char c2[maxInBufferSize]) {

  // helper function

  boolean returnValue = true;

  for (int i = 0; i <= maxInBufferSize - 1; i++) {
    if (c1[i] != c2[i]) {
      returnValue = false;
    }
  }
  return returnValue;

}



boolean StreamFile::setup(char fileName[12], ReplacementItems r) {
  if (DEBUG) Serial.println(F("StreamFile::setup() called"));
  f = SD.open(fileName);
  if (f) {
    refillInBuffer();
    refillOutBuffer();
  }
  replItems = r;
  return f;
}



boolean StreamFile::hasNext() {
  if (DEBUG) Serial.println(F("StreamFile::hasNext() called"));
  return (outBufferLevel > 0);
}



char StreamFile::readChar() {
  if (DEBUG) Serial.println(F("StreamFile::readChar() called"));
  char c;
  pullFromOutBuffer(&c);
  return c;
}



void StreamFile::closeFile() {
  if (DEBUG) Serial.println(F("StreamFile::closeFile() called"));
  f.close();
}

