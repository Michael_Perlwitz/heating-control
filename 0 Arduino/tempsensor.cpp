
#include <Arduino.h>
#include "tempsensor.h"

static const boolean DEBUG = false;



TempSensor::TempSensor() {
  if (DEBUG) Serial.println(F("TempSensor constructor called"));
}



float TempSensor::getUnweightedTemperature() {

  float rawReading = analogRead(thermistorPin);
  float voltage = rawReading / 1023 * VCC;
  float rTerm = voltage * rSerial / (VCC - voltage);

  const float T0 = kelvin + 25; // equivalent to 25°C
  const float B = 3950; // coefficient
  const float R0 = 4700; // resistance of thermistor at reference temperature
  const float selfHeating = 0.4;
  const float adjustmnentToRealTemp = 0;

  float temp =  1 / T0 + 1 / B * log(rTerm / R0); // Steinhart-Hart equation
  temp = 1 / temp - kelvin - selfHeating; // consider self-heating
  temp = temp + adjustmnentToRealTemp; // correct remaining measurement error

  return temp;
}



float TempSensor::getTemperature() {

  const byte numberOfMeasurements = 5;
  float temp = 0.0;
  
  for (byte i = 0; i <= numberOfMeasurements - 1; i++) {
    temp += getUnweightedTemperature()/numberOfMeasurements;
    delay(100);
  }
  
  return temp;

}

