
#ifndef MAIN_H
#define MAIN_H


#include "Display.h"
#include "TempSensor.h"
#include "Strategy.h"
#include "Valve.h"
#include "Time.h"
#include "EthernetShield.h"
#include "WebServer.h"
#include "NtpClient.h"
#include "Repo.h"
#include "Eprom.h"
#include "System.h"


class Main {

  private:

    Display display;
    TempSensor tempSensor;
    Strategy strategy;
    Valve valve;
    Time time;
    EthernetShield ethernetShield;
    //WebServer webServer;
    NtpClient ntpClient;
    //Repo repo;
    Eprom eprom;
    System system;
    
    byte mode; // see strategy.h for definition
    float currentTemp;
    byte targetTemp;
    byte position;
    float e;
    float esum;
    float y;
    const byte Kp = 60;
    const float Ki = 0.067; // per second
    const byte Ta = 1; // 1 second

    unsigned long lastReadTime;
    Eprom::Temperatures temp;


    void readInformation();
    void processInformation();
    void writeInformation();

    void serveWebRequests();
    void writeLogToRepo();


  public:

    void setupDependencies();
    void setup();
    void update();
    void update5m();
    void updateDaily();

};

#endif
































