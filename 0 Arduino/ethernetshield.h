

#ifndef ETHERNETSHIELD_H
#define ETHERNETSHIELD_H

#include <Ethernet2.h>
#include "ethernetshield.h"



class EthernetShield  {

  private:

    byte myMAC[6] = { 0x00, 0xAD, 0xBE, 0xEF, 0xFE, 0xE1 };
    IPAddress myIP = IPAddress(192, 168, 1, 222);


  public:

    void setup();

};


#endif
