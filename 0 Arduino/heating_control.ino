
#include <avr/wdt.h>
#include "main.h"
#include "eprom.h"


const boolean DEBUG = false;



Main *pMain;
unsigned long timeLastRun1s = 0;
unsigned long timeLastRun5m = 0;
unsigned long timeLastRunDaily = 0;



void setup() {

  wdt_disable();

  Eprom eprom;
  eprom.incrementBootCounter();

  Serial.begin(250000);
  while (!Serial) {
    delay(10);
  }
  Serial.println(F("Serial monitor initialized"));

  if (DEBUG) Serial.println(F("setup() called"));
  pMain = new Main();
  pMain->setup();

  delay(2L * 1000L);
  wdt_enable(WDTO_8S);

}



void loop() {

  if (millis() - timeLastRunDaily > (long) 24 * 60 * 60 * 1000) {
    if (DEBUG) Serial.println(F("loop daily called"));
    pMain->updateDaily();
    timeLastRunDaily = millis();
  }

  if (millis() - timeLastRun5m > (long) 5 * 60 * 1000) {
    if (DEBUG) Serial.println(F("loop 5m called"));
    pMain->update5m();
    timeLastRun5m = millis();
  }

  if (millis() - timeLastRun1s > 1000) {
    if (DEBUG) Serial.println(F("loop 1s called"));
    pMain->update();
    timeLastRun1s = millis();
  }

  wdt_reset();

}






