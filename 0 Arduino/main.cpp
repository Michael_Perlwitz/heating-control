
#include "main.h"
#include "webserver.h"
#include "streamfile.h"
//#include "strategy.h"

static const boolean DEBUG = false;





void Main::setup() {

  if (DEBUG) Serial.println(F("Main::setup() called"));

  currentTemp = 0;
  targetTemp = 0;
  e = 0;
  esum = 0;
  y = 0;

  lastReadTime = 0;

  ethernetShield.setup();

  ntpClient.setup();
  unsigned long unixTime = ntpClient.getUnixTime();
  if (DEBUG) Serial.println("Unix Time " + String(unixTime));
  time.setSystemTime(unixTime);

  if (DEBUG) Serial.println(F("Main::setup() complete"));
}



void Main::updateDaily() {
  if (DEBUG) Serial.println(F("Main::updateDaily() called"));
  unsigned long unixTime = ntpClient.getUnixTime();
  if (DEBUG) Serial.println("Unix Time " + String(unixTime));
  time.setSystemTime(unixTime);
}



void Main::update5m() {
  if (DEBUG) Serial.println(F("Main::update5m() called"));

  // Write log file
  writeLogToRepo();

  // Reset heating to auto once a day if in manual modes (i.e. fixed temperatures or max mode)
  float hourOfDay = time.getHourOfDay();
  if (hourOfDay >= 23.0 && hourOfDay <= 23.125) {
    if ((strategy.getMode() == Strategy::fixedTemp && strategy.getFixedTargetTemp() > strategy.defaultTemperature) || strategy.getMode() == Strategy::max) {
      strategy.setMode(Strategy::automatic, 0);
    }
  }
}



void Main::update() {
  if (DEBUG) Serial.println(F("Main::update() called"));
  readInformation();
  processInformation();
  writeInformation();
  if (DEBUG) Serial.println(F("Main::update() complete"));
}



void Main::readInformation() {

  // Get current temperature
  currentTemp = tempSensor.getTemperature();

  // Get target temperature
  temp = eprom.getSchedule();
  mode = strategy.getMode();
  targetTemp = strategy.getTemperatureTarget(time.getHourOfDay(), time.getUnixTime(), temp);

  // Check button
  display.checkButtonPressed();

}



void Main::processInformation() {
  e = targetTemp - currentTemp;
  esum = esum + e;
  esum = constrain(esum, -500, 500); // avoid wind-up effect
  float y = Kp * e + Ki * Ta * esum;

  switch (mode) {
    case Strategy::off:
      position = 0;
      break;
    case Strategy::automatic:
      position = constrain(y, 0, 100);
      break;
    case Strategy::fixedTemp:
      position = constrain(y, 0, 100);
      break;
    case Strategy::max:
      position = 100;
      break;
    default:
      position = 20;
  }

}



void Main::writeInformation() {

  // Write information to display
  display.setTemperatureActual(currentTemp);
  display.setTemperatureTarget(targetTemp);
  display.setTime(time.getHourOfDay());
  display.setControllerState(position /*, e, esum */);
  display.refreshDisplay();

  // set heating valve to calculated position
  valve.setValvePosition(position);

  // Serve web requests
  serveWebRequests();

}



void Main::serveWebRequests() {
  WebServer webServer;
  webServer.setup();

  WebServer::WebServerParameter p;

  p.currentTemp = currentTemp;
  p.targetTemp = targetTemp;
  p.error = e;
  p.errorSum = esum;
  p.valvePosition = position;
  p.hourOfDay = time.getHourOfDay();
  p.unixTime = time.getUnixTime();
  p.pStrategy = &strategy;
  p.temperatures = temp;
  p.uptime = system.getUptimeMillis();
  p.sram = system.getFreeSram();
  p.rebootCounter = eprom.getBootCounter();

  webServer.serveClientRequests(p);
}



void Main::writeLogToRepo() {
  Repo repo;
  repo.writeLog(time.getUnixTime(), currentTemp, targetTemp, position);
}

