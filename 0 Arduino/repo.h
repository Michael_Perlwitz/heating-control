

#ifndef REPO_H
#define REPO_H

#include <Arduino.h>
#include <SD.h>
#include "repo.h"


class Repo {


  public:

    void writeLog(unsigned long unixTime, float actualTemp, byte targetTemp, byte position);



    typedef struct {
      unsigned long unixTime;
      float actualTemp;
      byte targetTemp;
      byte position;
      boolean valid;
    } LineContent;

    enum { actualTempReport = 1, targetTempReport = 2, positionReport = 3 };

    File setupReadLog();
    boolean MovePointerToNLastLogEntries(File f, unsigned long nLastLogEntries);
    boolean readLogLine(File f, LineContent *lineContent);
    boolean getFormattedLogLine(File f, byte reportType, char *formattedLogLine);
    boolean endOfLog(File f);
    void closeLog(File f);

    void testReadLog();




    // functions for partial file loading
    static const byte patternSize = 4; // not zero-based
    boolean setup(char fileName[12], char fromPattern[patternSize], char toPattern[patternSize]);
    boolean hasNext();
    char readChar();
    void closeFile();

    void testPartialRead();

    static void keepAliveWatchdogTimer();



  private:

    enum { unixTime = 1, actualTemp = 2, targetTemp = 3, position = 4 };


    // variables for partial file loading
    File f;
    unsigned long startPosition;
    unsigned long endPosition;

    const byte numberOfCharsUnixTime = 10; // not zero-based
    const byte numberOfCharsUnixTimeAfterDecimalPoint = 0;
    const byte numberOfCharsActualTemp = 5; // not zero-based (sign, 2 digits before and one digit after decimal point
    const byte numberOfCharsActualTempAfterDecimalPoint = 1;
    const byte numberOfCharsTargetTemp = 3; // not zero-based (sign, 2 digits before and no digit after decimal point
    const byte numberOfCharsTargetTempAfterDecimalPoint = 0;
    const byte numberOfCharsPosition = 3; // not zero-based (no sign, 3 digits before and no digit after decimal point
    const byte numberOfCharsPositionAfterDecimalPoint = 0; 
    
};

#endif







