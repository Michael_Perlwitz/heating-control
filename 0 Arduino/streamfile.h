
#ifndef STREAMFILE_H
#define STREAMFILE_H

#include <SD.h>

class StreamFile {

  private:

    static const byte patternToBeReplacedSize = 4; // not zero-based
    static const byte maxReplacementStringSize = 10; // not zero-based
    static const byte maxReplacementItems = 33; // not zero-based // !!! Value has to be the number of used items, otherwise program freezes !!!

    static const byte maxInBufferSize = patternToBeReplacedSize; // not zero-based
    static const byte maxOutBufferSize = 2 * maxReplacementStringSize; // not zero-based

    typedef struct {
      char patternToBeReplaced[patternToBeReplacedSize];
      char replacementString[maxReplacementStringSize];
      byte length;
    } ReplacementItem;

    File f;



    char inBuffer[4];
    char outBuffer[20];
    byte inBufferLevel;
    byte outBufferLevel;

    byte pullFromOutBuffer(char *c);
    void refillOutBuffer();
    byte pullWithReplacementsFromInbuffer(char *buf);
    byte pullFromInbuffer(char *c);
    void flushInBuffer();
    void refillInBuffer();
    byte patternMatch(char buf[4]);
    byte getReplacementString(char *replacementString, int matchID);

    


  public:

    typedef struct {
      ReplacementItem items[maxReplacementItems];
    } ReplacementItems;

    ReplacementItems replItems;

    StreamFile();
    boolean setup(char fileName[12], ReplacementItems r);
    boolean hasNext();
    char readChar();
    void closeFile();

    static boolean charArrayCompare(char c1[4], char c2[4]);

};

#endif
