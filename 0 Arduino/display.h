
#ifndef DISPLAY_H
#define DISPLAY_H

#include <Arduino.h>
#include <LiquidCrystal.h>

#include "display.h"



class Display {


  private:

    const byte BUTTON = A1;

    byte prevButton;
    byte newButton;
    byte page;
    
    char displayContent[5][2][16]; // 5 pages, 2 lines, 16 characters
    
    LiquidCrystal lcd = LiquidCrystal(8, 7, 6, 5, 3, 2);


  public:

    Display();
    
    void checkButtonPressed();
    
    void setTemperatureActual(float temp);
    void setTemperatureTarget(float temp);
    void setTime(float h);
    void setControllerState(float valvePosition /*, float error, float errorSum */);
    
    void refreshDisplay();

};

#endif




