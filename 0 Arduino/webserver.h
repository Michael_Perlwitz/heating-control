
#ifndef WEBSERVER_H
#define WEBSERVER_H


#include <Ethernet2.h>

#include "repo.h"
#include "streamfile.h"
#include "strategy.h"


class WebServer {

  public:

    typedef struct {
      float currentTemp;
      byte targetTemp;
      float error;
      float errorSum;
      byte valvePosition;
      float hourOfDay;
      unsigned long unixTime;
      Strategy *pStrategy;
      Eprom::Temperatures temperatures;
      unsigned long uptime;
      int sram;
      byte rebootCounter;
    } WebServerParameter;

    void setup();
    void serveClientRequests(WebServerParameter p);



  private:

    EthernetServer server = EthernetServer(80);

    static const byte maxFileNameLength = 15;
    static const byte maxParamNameLength = 15;
    static const byte maxParamValueLength = 20;
    static const byte maxParams = 15;

    typedef struct {
      char name[maxParamNameLength];
      char value[maxParamValueLength];
      boolean valid;
    } Param;

    typedef struct {
      char fileName[maxFileNameLength];
      Param params[maxParams];
    } DecompUrl;

    const char credential[4] = "tim";

    boolean readRequestHeader(EthernetClient client, char *headerContent);
    void getUrlFromHeader(char *headerContent, char *url);
    void decomposeUrl(char *url, DecompUrl *decompUrl);
    void callActionHandler(EthernetClient client, WebServer::WebServerParameter p, DecompUrl *decompUrl);
    void sendHttpResponseHeader(EthernetClient client);
    void sendHtmlBody(EthernetClient client, WebServer::WebServerParameter p, char url[maxFileNameLength]);


};


#endif
