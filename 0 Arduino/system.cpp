
#include <Arduino.h>
#include "system.h"



unsigned long System::getUptimeMillis() {
  return millis();
}


int System::getFreeSram() {
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}

