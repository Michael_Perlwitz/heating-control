
#include "display.h"
#include <LiquidCrystal.h>

static const boolean DEBUG = false;



Display::Display() {

  if (DEBUG) Serial.println(F("Display constructor called"));

  prevButton = 0;
  newButton = 0;
  page = 0;

  // set up the LCD's number of columns and rows
  lcd.begin(16, 2);

  // set up button
  pinMode(BUTTON, INPUT);

}





void Display::checkButtonPressed() {
  newButton = digitalRead(BUTTON);
  if (newButton == HIGH && prevButton == LOW) {
    page = (page + 1) % 5;
    delay(10);
  }
  prevButton = newButton;
}



void Display::setTemperatureActual(float temp) {
  strcpy(displayContent[0][0], "Temperatur Ist:");
  dtostrf(temp, 4, 2, displayContent[0][1]);
}



void Display::setTemperatureTarget(float temp) {
  strcpy(displayContent[1][0], "Temperatur Soll:");
  dtostrf(temp, 4, 2, displayContent[1][1]);
}



void Display::setTime(float h) {
  strcpy(displayContent[2][0], "Systemzeit:");
  dtostrf(h, 4, 2, displayContent[2][1]);
}



void Display::setControllerState(float valvePosition /*, float error, float errorSum */) {
  strcpy(displayContent[3][0], "Ventil Pos:");
  dtostrf(valvePosition, 4, 2, displayContent[3][1]);
}




void Display::refreshDisplay() {

  lcd.clear();

  lcd.setCursor(0, 0);
  lcd.print(displayContent[page][0]);
  lcd.setCursor(0, 1);
  lcd.print(displayContent[page][1]);

}






