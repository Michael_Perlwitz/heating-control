
#ifndef NTPCLIENT_H
#define NTPCLIENT_H


#include <EthernetUdp2.h>
#include "ntpclient.h"


class NtpClient {

  private:
    unsigned int localPort = 8888;       // local port to listen for UDP packets
    char timeServer[16] = "de.pool.ntp.org";
    byte packetBuffer[48]; //buffer to hold incoming and outgoing packets
    EthernetUDP Udp;


  public:
    void setup();
    unsigned long getUnixTime();

};


#endif
