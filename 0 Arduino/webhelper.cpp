
#include "webhelper.h"
#include "webserver.h"



void WebHelper::prepareReplacementItems(StreamFile::ReplacementItems *r, WebServer::WebServerParameter p) {
  
   {
    char result[4];
    dtostrf(p.currentTemp, 4, 1, result);
    strncpy(r->items[0].patternToBeReplaced, "[01]", 4);
    strncpy(r->items[0].replacementString, result, 4);
    r->items[0].length = 4;
  }
  {
    char result[4];
    dtostrf(p.targetTemp, 4, 1, result);
    strncpy(r->items[1].patternToBeReplaced, "[02]", 4);
    strncpy(r->items[1].replacementString, result, 4);
    r->items[1].length = 4;
  }
  {
    char result[5];
    dtostrf(p.error, 5, 1, result);
    strncpy(r->items[2].patternToBeReplaced, "[03]", 4);
    strncpy(r->items[2].replacementString, result, 5);
    r->items[2].length = 5;
  }
  {
    char result[5];
    dtostrf(p.errorSum, 4, 0, result);
    strncpy(r->items[3].patternToBeReplaced, "[04]", 4);
    strncpy(r->items[3].replacementString, result, 4);
    r->items[3].length = 4;
  }
  {
    char result[3];
    dtostrf(p.valvePosition, 3, 0, result);
    strncpy(r->items[4].patternToBeReplaced, "[05]", 4);
    strncpy(r->items[4].replacementString, result, 3);
    r->items[4].length = 3;
  }
  {
    char result[5];
    dtostrf(p.hourOfDay, 5, 2, result);
    strncpy(r->items[5].patternToBeReplaced, "[06]", 4);
    strncpy(r->items[5].replacementString, result, 5);
    r->items[5].length = 5;
  }


  byte mode = (p.pStrategy)->getMode();
  byte temp = (p.pStrategy)->getFixedTargetTemp();

  {
    const byte item = 6;
    const byte lengthPattern = 4;
    const char patternToBeReplaced[lengthPattern + 1] = "[07]";
    if (mode == 0) {
      strncpy(r->items[item].patternToBeReplaced, patternToBeReplaced, lengthPattern);
      strncpy(r->items[item].replacementString, "checked", 7);
      r->items[item].length = 7;
    } else {
      strncpy(r->items[item].patternToBeReplaced, patternToBeReplaced, lengthPattern);
      strncpy(r->items[item].replacementString, " ", 1);
      r->items[item].length = 1;
    }
  }
  {
    const byte item = 7;
    const byte lengthPattern = 4;
    const char patternToBeReplaced[lengthPattern + 1] = "[08]";
    if (mode == 1) {
      strncpy(r->items[item].patternToBeReplaced, patternToBeReplaced, lengthPattern);
      strncpy(r->items[item].replacementString, "checked", 7);
      r->items[item].length = 7;
    } else {
      strncpy(r->items[item].patternToBeReplaced, patternToBeReplaced, lengthPattern);
      strncpy(r->items[item].replacementString, " ", 1);
      r->items[item].length = 1;
    }
  }
  {
    const byte item = 8;
    const byte lengthPattern = 4;
    const char patternToBeReplaced[lengthPattern + 1] = "[09]";
    if (mode == 9) {
      strncpy(r->items[item].patternToBeReplaced, patternToBeReplaced, lengthPattern);
      strncpy(r->items[item].replacementString, "checked", 7);
      r->items[item].length = 7;
    } else {
      strncpy(r->items[item].patternToBeReplaced, patternToBeReplaced, lengthPattern);
      strncpy(r->items[item].replacementString, " ", 1);
      r->items[item].length = 1;
    }
  }
  {
    const byte item = 9;
    const byte lengthPattern = 4;
    const char patternToBeReplaced[lengthPattern + 1] = "[10]";
    if (mode == 2 && temp == 19) {
      strncpy(r->items[item].patternToBeReplaced, patternToBeReplaced, lengthPattern);
      strncpy(r->items[item].replacementString, "checked", 7);
      r->items[item].length = 7;
    } else {
      strncpy(r->items[item].patternToBeReplaced, patternToBeReplaced, lengthPattern);
      strncpy(r->items[item].replacementString, " ", 1);
      r->items[item].length = 1;
    }
  }
  {
    const byte item = 10;
    const byte lengthPattern = 4;
    const char patternToBeReplaced[lengthPattern + 1] = "[11]";
    if (mode == 2 && temp == 20) {
      strncpy(r->items[item].patternToBeReplaced, patternToBeReplaced, lengthPattern);
      strncpy(r->items[item].replacementString, "checked", 7);
      r->items[item].length = 7;
    } else {
      strncpy(r->items[item].patternToBeReplaced, patternToBeReplaced, lengthPattern);
      strncpy(r->items[item].replacementString, " ", 1);
      r->items[item].length = 1;
    }
  }
  {
    const byte item = 11;
    const byte lengthPattern = 4;
    const char patternToBeReplaced[lengthPattern + 1] = "[12]";
    if (mode == 2 && temp == 21) {
      strncpy(r->items[item].patternToBeReplaced, patternToBeReplaced, lengthPattern);
      strncpy(r->items[item].replacementString, "checked", 7);
      r->items[item].length = 7;
    } else {
      strncpy(r->items[item].patternToBeReplaced, patternToBeReplaced, lengthPattern);
      strncpy(r->items[item].replacementString, " ", 1);
      r->items[item].length = 1;
    }
  }
  {
    const byte item = 12;
    const byte lengthPattern = 4;
    const char patternToBeReplaced[lengthPattern + 1] = "[13]";
    if (mode == 2 && temp == 22) {
      strncpy(r->items[item].patternToBeReplaced, patternToBeReplaced, lengthPattern);
      strncpy(r->items[item].replacementString, "checked", 7);
      r->items[item].length = 7;
    } else {
      strncpy(r->items[item].patternToBeReplaced, patternToBeReplaced, lengthPattern);
      strncpy(r->items[item].replacementString, " ", 1);
      r->items[item].length = 1;
    }
  }
  {
    const byte item = 13;
    const byte lengthPattern = 4;
    const char patternToBeReplaced[lengthPattern + 1] = "[14]";
    if (mode == 2 && temp == 23) {
      strncpy(r->items[item].patternToBeReplaced, patternToBeReplaced, lengthPattern);
      strncpy(r->items[item].replacementString, "checked", 7);
      r->items[item].length = 7;
    } else {
      strncpy(r->items[item].patternToBeReplaced, patternToBeReplaced, lengthPattern);
      strncpy(r->items[item].replacementString, " ", 1);
      r->items[item].length = 1;
    }
  }


  {
    char result[2];
    const byte item = 14;
    dtostrf(p.temperatures.item[0].fromHour, 2, 0, result);
    strncpy(r->items[item].patternToBeReplaced, "[15]", 4);
    strncpy(r->items[item].replacementString, result, 2);
    r->items[item].length = 2;
  }
  {
    char result[2];
    const byte item = 15;
    dtostrf(p.temperatures.item[0].toHour, 2, 0, result);
    strncpy(r->items[item].patternToBeReplaced, "[16]", 4);
    strncpy(r->items[item].replacementString, result, 2);
    r->items[item].length = 2;
  }
  {
    char result[2];
    const byte item = 16;
    dtostrf(p.temperatures.item[0].temp, 2, 0, result);
    strncpy(r->items[item].patternToBeReplaced, "[17]", 4);
    strncpy(r->items[item].replacementString, result, 2);
    r->items[item].length = 2;
  }

  {
    char result[2];
    const byte item = 17;
    dtostrf(p.temperatures.item[1].fromHour, 2, 0, result);
    strncpy(r->items[item].patternToBeReplaced, "[18]", 4);
    strncpy(r->items[item].replacementString, result, 2);
    r->items[item].length = 2;
  }
  {
    char result[2];
    const byte item = 18;
    dtostrf(p.temperatures.item[1].toHour, 2, 0, result);
    strncpy(r->items[item].patternToBeReplaced, "[19]", 4);
    strncpy(r->items[item].replacementString, result, 2);
    r->items[item].length = 2;
  }
  {
    char result[2];
    const byte item = 19;
    dtostrf(p.temperatures.item[1].temp, 2, 0, result);
    strncpy(r->items[item].patternToBeReplaced, "[20]", 4);
    strncpy(r->items[item].replacementString, result, 2);
    r->items[item].length = 2;
  }


  {
    char result[2];
    const byte item = 20;
    dtostrf(p.temperatures.item[2].fromHour, 2, 0, result);
    strncpy(r->items[item].patternToBeReplaced, "[21]", 4);
    strncpy(r->items[item].replacementString, result, 2);
    r->items[item].length = 2;
  }
  {
    char result[2];
    const byte item = 21;
    dtostrf(p.temperatures.item[2].toHour, 2, 0, result);
    strncpy(r->items[item].patternToBeReplaced, "[22]", 4);
    strncpy(r->items[item].replacementString, result, 2);
    r->items[item].length = 2;
  }
  {
    char result[2];
    const byte item = 22;
    dtostrf(p.temperatures.item[2].temp, 2, 0, result);
    strncpy(r->items[item].patternToBeReplaced, "[23]", 4);
    strncpy(r->items[item].replacementString, result, 2);
    r->items[item].length = 2;
  }


  {
    char result[2];
    const byte item = 23;
    dtostrf(p.temperatures.item[3].fromHour, 2, 0, result);
    strncpy(r->items[item].patternToBeReplaced, "[24]", 4);
    strncpy(r->items[item].replacementString, result, 2);
    r->items[item].length = 2;
  }
  {
    char result[2];
    const byte item = 24;
    dtostrf(p.temperatures.item[3].toHour, 2, 0, result);
    strncpy(r->items[item].patternToBeReplaced, "[25]", 4);
    strncpy(r->items[item].replacementString, result, 2);
    r->items[item].length = 2;
  }
  {
    char result[2];
    const byte item = 25;
    dtostrf(p.temperatures.item[3].temp, 2, 0, result);
    strncpy(r->items[item].patternToBeReplaced, "[26]", 4);
    strncpy(r->items[item].replacementString, result, 2);
    r->items[item].length = 2;
  }

  {
    char result[6];
    const byte item = 26;
    dtostrf(p.uptime / (60 * 60 * 1000.0), 3, 2, result);
    strncpy(r->items[item].patternToBeReplaced, "[27]", 4);
    strncpy(r->items[item].replacementString, result, 6);
    r->items[item].length = 6;
  }

  {
    char result[3];
    const byte item = 27;
    dtostrf(p.rebootCounter, 3, 0, result);
    strncpy(r->items[item].patternToBeReplaced, "[28]", 4);
    strncpy(r->items[item].replacementString, result, 3);
    r->items[item].length = 3;
  }

  {
    char result[5];
    const byte item = 28;
    dtostrf(p.sram, 5, 0, result);
    strncpy(r->items[item].patternToBeReplaced, "[29]", 4);
    strncpy(r->items[item].replacementString, result, 5);
    r->items[item].length = 5;
  }

  {
    Strategy::LatLng latLng = (p.pStrategy)->getUserCoordinates(0);
    float lat = latLng.lat;
    float lng = latLng.lng;
    unsigned long uploadTime = latLng.uploadTime;

    if (p.unixTime - uploadTime <= Strategy::maxAge) {
      {
        char result[8];
        const byte item = 29;
        dtostrf(lat, 8, 4, result);
        strncpy(r->items[item].patternToBeReplaced, "[30]", 4);
        strncpy(r->items[item].replacementString, result, 8);
        r->items[item].length = 8;
      }
      {
        char result[8];
        const byte item = 30;
        dtostrf(lng, 8, 4, result);
        strncpy(r->items[item].patternToBeReplaced, "[31]", 4);
        strncpy(r->items[item].replacementString, result, 8);
        r->items[item].length = 8;
      }
    } else {
      {
        const byte item = 29;
        strncpy(r->items[item].patternToBeReplaced, "[30]", 4);
        strncpy(r->items[item].replacementString, "[30]", 4);
        r->items[item].length = 4;
      }
      {
        const byte item = 30;
        strncpy(r->items[item].patternToBeReplaced, "[31]", 4);
        strncpy(r->items[item].replacementString, "[31]", 4);
        r->items[item].length = 4;
      }
    }
  }
  {
    Strategy::LatLng latLng = (p.pStrategy)->getUserCoordinates(1);
    float lat = latLng.lat;
    float lng = latLng.lng;
    unsigned long uploadTime = latLng.uploadTime;

    if (p.unixTime - uploadTime <= Strategy::maxAge) {
      {
        char result[8];
        const byte item = 31;
        dtostrf(lat, 8, 4, result);
        strncpy(r->items[item].patternToBeReplaced, "[32]", 4);
        strncpy(r->items[item].replacementString, result, 8);
        r->items[item].length = 8;
      }
      {
        char result[8];
        const byte item = 32;
        dtostrf(lng, 8, 4, result);
        strncpy(r->items[item].patternToBeReplaced, "[33]", 4);
        strncpy(r->items[item].replacementString, result, 8);
        r->items[item].length = 8;
      }
    } else {
      {
        const byte item = 31;
        strncpy(r->items[item].patternToBeReplaced, "[32]", 4);
        strncpy(r->items[item].replacementString, "[32]", 4);
        r->items[item].length = 4;
      }
      {
        const byte item = 32;
        strncpy(r->items[item].patternToBeReplaced, "[33]", 4);
        strncpy(r->items[item].replacementString, "[33]", 4);
        r->items[item].length = 4;
      }
    }
  }

}

