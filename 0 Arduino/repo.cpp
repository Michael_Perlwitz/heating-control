
#include <avr/wdt.h>
#include <SD.h>
#include "repo.h"
#include "streamfile.h"



void Repo::writeLog(unsigned long unixTime, float actualTemp, byte targetTemp, byte position) {

  const boolean LOCAL_DEBUG = false;

  if (LOCAL_DEBUG) Serial.println("Entering writeLog...");
  File f = SD.open("log.txt", FILE_WRITE);

  char chrUnixTime[numberOfCharsUnixTime + 1];
  char chrActualTemp[numberOfCharsActualTemp + 1];
  char chrTargetTemp[numberOfCharsTargetTemp + 1];
  char chrPosition[numberOfCharsPosition + 1];
  char totalLine[numberOfCharsUnixTime + numberOfCharsActualTemp + numberOfCharsTargetTemp + numberOfCharsPosition + 3]; // 3 semicolons

  dtostrf(unixTime,     numberOfCharsUnixTime,    numberOfCharsUnixTimeAfterDecimalPoint,     chrUnixTime);
  dtostrf(actualTemp,   numberOfCharsActualTemp,  numberOfCharsActualTempAfterDecimalPoint,   chrActualTemp);
  dtostrf(targetTemp,   numberOfCharsTargetTemp,  numberOfCharsTargetTempAfterDecimalPoint,   chrTargetTemp);
  dtostrf(position,     numberOfCharsPosition,    numberOfCharsPositionAfterDecimalPoint,     chrPosition);

  if (LOCAL_DEBUG) {
    Serial.print("unixTime: "); Serial.println(unixTime);
    Serial.print("actualTemp: "); Serial.println(actualTemp);
    Serial.print("targetTemp: "); Serial.println(targetTemp);
    Serial.print("position: "); Serial.println(position);

    Serial.print("chrUnixTime: "); Serial.println(chrUnixTime);
    Serial.print("chrActualTemp: "); Serial.println(chrActualTemp);
    Serial.print("chrTargetTemp: "); Serial.println(chrTargetTemp);
    Serial.print("chrPosition: "); Serial.println(chrPosition);
  }

  const char s[2] = ";";
  strcpy(totalLine, chrUnixTime);
  strcat(totalLine, s);
  strcat(totalLine, chrActualTemp);
  strcat(totalLine, s);
  strcat(totalLine, chrTargetTemp);
  strcat(totalLine, s);
  strcat(totalLine, chrPosition);

  if (LOCAL_DEBUG) {
    Serial.print("Log file entry: "); Serial.println(totalLine);
  }

  f.println(totalLine);
  f.close();

}



File Repo::setupReadLog() {
  const boolean LOCAL_DEBUG = false;
  if (LOCAL_DEBUG) Serial.println("setupReadLog() called");
  return SD.open("log.txt");
}



boolean Repo::MovePointerToNLastLogEntries(File f, unsigned long nLastLogEntries) {

  const byte charsPerLine = 23;
  unsigned long goToPosition = max(0, ((long) f.size()) - ((long) nLastLogEntries) * charsPerLine);
  f.seek(goToPosition);

  // move to new line
  while (f.available() && f.read() != '\n') {
    ;
  }

  return f.available();
}



boolean Repo::readLogLine(File f, LineContent *lineContent) {

  const boolean LOCAL_DEBUG = false;
  if (LOCAL_DEBUG) Serial.println("readLogLine() called");

  // avoid time outs in case of long files or lots of serial debugging...
  keepAliveWatchdogTimer();

  char chrUnixTime[11]; // 10 characters and \0
  char chrActualTemp[6]; // 5 characters and \0
  char chrTargetTemp[4]; // 3 characters and \0
  char chrPosition[4]; // 3 characters and \0

  byte currentVariable = 1;
  byte index = 0;

  char cRead = ' ';
  char cWrite = ' ';
  boolean writeCharacter = false;
  boolean progressToNextVariable = false;

  boolean done = false;

  while (!done) {

    if (!endOfLog(f)) {
      cRead = f.read();
      if (LOCAL_DEBUG) {
        Serial.print("Read char from file: "); Serial.println(cRead);
      }
    } else {
      done = true;
      if (LOCAL_DEBUG) {
        Serial.println("Done");
      }
    }

    if (cRead == ';') {
      cWrite = '\0';
      writeCharacter = true;
      progressToNextVariable = true;
    } else if (cRead == '\n') {
      cWrite = '\0';
      writeCharacter = true;
      done = true;
    } else if (cRead == '\r') {
      cWrite = ' ';
      writeCharacter = false;
    } else {
      cWrite = cRead;
      writeCharacter = true;
    }

    if (writeCharacter) {
      switch (currentVariable) {
        case unixTime:
          if (LOCAL_DEBUG) {
            Serial.print("chrUnixTime: index: "); Serial.println(index);
            Serial.print("character: "); Serial.println(cWrite);
          }
          chrUnixTime[index] = cWrite;
          index++;
          break;
        case actualTemp:
          if (LOCAL_DEBUG) {
            Serial.print("chrActualTemp: index: "); Serial.println(index);
            Serial.print("character: "); Serial.println(cWrite);
          }
          chrActualTemp[index] = cWrite;
          index++;
          break;
        case targetTemp:
          if (LOCAL_DEBUG) {
            Serial.print("chrTargetTemp: index: "); Serial.println(index);
            Serial.print("character: "); Serial.println(cWrite);
          }
          chrTargetTemp[index] = cWrite;
          index++;
          break;
        case position:
          if (LOCAL_DEBUG) {
            Serial.print("chrPosition: index: "); Serial.println(index);
            Serial.print("character: "); Serial.println(cWrite);
          }
          chrPosition[index] = cWrite;
          index++;
          break;
        default:
          ; // ignore, if none of the above
      }
    }

    if (progressToNextVariable) {
      currentVariable++;
      index = 0;
      progressToNextVariable = false;
    }
  }

  if (LOCAL_DEBUG) {
    Serial.print("chrUnixTime: "); Serial.println(chrUnixTime);
    Serial.print("chrActualTemp: "); Serial.println(chrActualTemp);
    Serial.print("chrTargetTemp: "); Serial.println(chrTargetTemp);
    Serial.print("chrPosition: "); Serial.println(chrPosition);
  }

  // convert chars to final data types and do plausibility checks

  unsigned long unixTime = atol(chrUnixTime);
  unsigned long unixTimeLowerLimit = 1483228800L; // 2017-01-01
  unsigned long unixTimeUpperLimit = 1640995200L; // 2022-01-01
  boolean unixTimeValid = (currentVariable >= 1 && unixTime > unixTimeLowerLimit && unixTime < unixTimeUpperLimit);

  float actualTemp = atof(chrActualTemp);
  float targetTemp = atof(chrTargetTemp);
  int tempLowerLimit = -10;
  int tempUpperLimit = 40;
  boolean actualTempValid = (currentVariable >= 2 && actualTemp > tempLowerLimit && actualTemp < tempUpperLimit);
  boolean targetTempValid = (currentVariable >= 3 && targetTemp > tempLowerLimit && targetTemp < tempUpperLimit);

  byte position = (byte) atoi(chrPosition);
  boolean positionValid = (currentVariable >= 4 && position <= 100); // check if >=0 not necessary as it is granted by byte type


  boolean valid = (unixTimeValid && actualTempValid && targetTempValid && positionValid);

  if (LOCAL_DEBUG) {
    Serial.print("unixTime: "); Serial.println(unixTime);
    Serial.print("actualTemp: "); Serial.println(actualTemp);
    Serial.print("targetTemp: "); Serial.println(targetTemp);
    Serial.print("position: "); Serial.println(position);
    Serial.print("valid: "); Serial.println(valid);
  }

  // save return values to *struct
  lineContent->valid = valid;
  if (valid) {
    lineContent->unixTime = unixTime;
    lineContent->actualTemp = actualTemp;
    lineContent->targetTemp = targetTemp;
    lineContent->position = position;
  }

  if (LOCAL_DEBUG) {
    Serial.print("lineContent->unixTime: "); Serial.println(lineContent->unixTime);
    Serial.print("lineContent->actualTemp: "); Serial.println(lineContent->actualTemp);
    Serial.print("lineContent->targetTemp: "); Serial.println(lineContent->targetTemp);
    Serial.print("lineContent->position: "); Serial.println(lineContent->position);
    Serial.print("lineContent->valid: "); Serial.println(lineContent->valid);
  }

  return valid;

}






boolean Repo::getFormattedLogLine(File f, byte reportType, char *formattedLogLine) {

  const boolean LOCAL_DEBUG = false;
  if (LOCAL_DEBUG) Serial.println("getFormattedLogLine() called");


  LineContent lineContent;
  lineContent.valid = false;

  readLogLine(f, &lineContent);

  if (LOCAL_DEBUG) {
    Serial.println(lineContent.unixTime);
    Serial.println(lineContent.actualTemp);
    Serial.println(lineContent.targetTemp);
    Serial.println(lineContent.position);
    Serial.println(lineContent.valid);
  }

  if (lineContent.valid) {
    char chrUnixTime[10];
    dtostrf(lineContent.unixTime, 10, 0, chrUnixTime);
    char chrActualTemp[5];
    dtostrf(lineContent.actualTemp, 5, 1, chrActualTemp);
    char chrTargetTemp[3];
    dtostrf(lineContent.targetTemp, 3, 0, chrTargetTemp);
    char chrPosition[3];
    dtostrf(lineContent.position, 3, 0, chrPosition);

    if (LOCAL_DEBUG) {
      Serial.println(chrUnixTime);
      Serial.println(chrActualTemp);
      Serial.println(chrTargetTemp);
      Serial.println(chrPosition);
    }


    switch (reportType) {
      case actualTempReport:
        strcpy(formattedLogLine, "[");
        strcat(formattedLogLine, chrUnixTime);
        strcat(formattedLogLine, "000, ");
        strcat(formattedLogLine, chrActualTemp);
        strcat(formattedLogLine, "]\0");
        break;
      case targetTempReport:
        strcpy(formattedLogLine, "[");
        strcat(formattedLogLine, chrUnixTime);
        strcat(formattedLogLine, "000, ");
        strcat(formattedLogLine, chrTargetTemp);
        strcat(formattedLogLine, "]\0");
        break;
      case positionReport:
        strcpy(formattedLogLine, "[");
        strcat(formattedLogLine, chrUnixTime);
        strcat(formattedLogLine, "000, ");
        strcat(formattedLogLine, chrPosition);
        strcat(formattedLogLine, "]\0");
        break;
      default:
        ; // ignore unspecified report types
    }
    if (LOCAL_DEBUG) Serial.println(formattedLogLine);
  }

  return lineContent.valid;
}




boolean Repo::endOfLog(File f) {
  const boolean LOCAL_DEBUG = false;

  if (LOCAL_DEBUG) Serial.println("endOfLog() called");
  unsigned long position = f.position();
  unsigned long size = f.size();
  return (position >= size);
};



void Repo::closeLog(File f) {
  const boolean LOCAL_DEBUG = false;
  if (LOCAL_DEBUG) Serial.println("closeLog() called");
  f.close();
}


void Repo::testReadLog() {

  const boolean LOCAL_DEBUG = true;
  if (LOCAL_DEBUG) Serial.println("testReadLog() called");

  File f = setupReadLog();
  boolean first = true;

  if (MovePointerToNLastLogEntries(f, 10)) {
    while (!endOfLog(f)) {
      char reportLine[30];
      getFormattedLogLine(f, actualTempReport, reportLine);
      if (first) {
        if (LOCAL_DEBUG) Serial.print(reportLine);
        first = false;
      } else {
        if (LOCAL_DEBUG) {
          Serial.print(",\n");
          Serial.print(reportLine);
        }
      }
    }
  }

}



















boolean Repo::setup(char fileName[12], char fromPattern[patternSize], char toPattern[patternSize]) {

  const boolean LOCAL_DEBUG = false;
  if (LOCAL_DEBUG) Serial.println(F("Repo::setup() called"));

  f = SD.open(fileName);

  unsigned long fileSize = f.size();
  startPosition = 0;
  endPosition = fileSize;
  char buffer[patternSize] = { ' ' }; // , ' ', ' ', ' ' };

  while (f.available()) {

    // read char
    char c = f.read();

    // shift buffer and append char
    for (byte i = 0; i <= patternSize - 2; i++) {
      buffer[i] = buffer[i + 1];
    }
    buffer[patternSize - 1] = c;

    // if no startPosition yet, compare buffer to fromPattern and get startPosition
    if (fromPattern != NULL && startPosition == 0 && StreamFile::charArrayCompare(buffer, fromPattern)) {
      startPosition = f.position();
    }

    // if no endPosition yet, compare buffer to toPattern and get endPosition
    if (toPattern != NULL && endPosition == fileSize && StreamFile::charArrayCompare(buffer, toPattern)) {
      endPosition = f.position() - patternSize - 1;
    }
  }

  if (LOCAL_DEBUG) {
    Serial.println(startPosition);
    Serial.println(endPosition);
  }

  // set cursor to begin of file
  f.seek(startPosition);

  return f;
}



boolean Repo::hasNext() {
  const boolean LOCAL_DEBUG = false;
  if (LOCAL_DEBUG) Serial.println(F("Repo::hasNext() called"));
  unsigned long position = f.position();
  if (LOCAL_DEBUG) Serial.println(position);
  return (position >= startPosition && position < endPosition);
}



char Repo::readChar() {
  const boolean LOCAL_DEBUG = false;
  if (LOCAL_DEBUG) Serial.println(F("Repo::readChar() called"));

  char c = '\0';
  unsigned long position = f.position();
  if (position >= startPosition && position <= endPosition) {
    c = f.read();
  }

  return c;
}



void Repo::closeFile() {
  const boolean LOCAL_DEBUG = false;
  if (LOCAL_DEBUG) Serial.println(F("Repo::closeFile() called"));
  f.close();
}



void Repo::testPartialRead() {

  const boolean LOCAL_DEBUG = true;
  if (LOCAL_DEBUG) Serial.println(F("Repo::testPartialRead() called"));

  setup((char *) "index.htm", (char *) "[01]", (char *) "[02]");

  while (hasNext()) {
    char c = readChar();
    Serial.println(c);
  }
  closeFile();
}



void Repo::keepAliveWatchdogTimer() {
  wdt_reset();
}



