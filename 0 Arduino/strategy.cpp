
#include <Arduino.h>

#include "strategy.h"


static const boolean DEBUG = false;


Strategy::Strategy() {
  mode = automatic;
  fixedTargetTemp = defaultTemperature;

  for (byte i = 0; i <= numberOfUsers - 1; i++) {
    userCoordinates[i].lat = latBesigheim;
    userCoordinates[i].lng = lngBesigheim;
    userCoordinates[i].uploadTime = 0;
  }

}


void Strategy::setMode(byte newMode, byte newFixedTargetTemp) {
  if (DEBUG) Serial.println("newMode: " + String(newMode) + ", newFixedTargetTemp: " + String(newFixedTargetTemp));
  mode = newMode;
  if (newMode == 2) fixedTargetTemp = newFixedTargetTemp;
}



byte Strategy::getMode() {
  return mode;
}



byte Strategy::getFixedTargetTemp() {
  return fixedTargetTemp;
}



byte Strategy::getTemperatureTarget(float h, unsigned long unixTime, Eprom::Temperatures temp) {

  const boolean LOCAL_DEBUG = false;
  byte returnValue = minTemp;
  
  if (DEBUG) Serial.println("getTemperatureTarget");

  if (mode == off) {
    returnValue = minTemp; // return minTemp to avoid freezing

  } else if (mode == automatic) {
    for (byte i = 0; i <= 4; i++) {
      if (DEBUG) Serial.println(h);
      if (DEBUG) Serial.println(i);
      if (DEBUG) Serial.println(temp.item[i].fromHour );
      if (DEBUG) Serial.println(temp.item[i].toHour);
      if (DEBUG) Serial.println(temp.item[i].temp);

      if (h >= temp.item[i].fromHour && h <= temp.item[i].toHour) {
        if (DEBUG) Serial.println("match!");
        // get target temperature according to schedule
        byte targetTempSchedule = temp.item[i].temp;
        // get min distance of users
        unsigned int minDistance = getMinDistance(unixTime);
        byte tempCorrection = round(minDistance * tempCorrPerKm);
        // adjust target temp in line with min distance
        byte targetTempPreBoundaries = targetTempSchedule - tempCorrection;
        // ensure that min temp does not fall below threshold
        byte targetTemp = max(targetTempPreBoundaries, minTemp);
        if (LOCAL_DEBUG) {
          Serial.print("targetTempSchedule: "); Serial.println(targetTempSchedule);
          Serial.print("minDistance: "); Serial.println(minDistance);
          Serial.print("tempCorrection: "); Serial.println(tempCorrection);
          Serial.print("targetTempPreBoundaries: "); Serial.println(targetTempPreBoundaries);
          Serial.print("targetTemp: "); Serial.println(targetTemp);
        }

        returnValue = targetTemp;
        break;
      }
    }

  } else if (mode == fixedTemp) {
    returnValue = fixedTargetTemp;

  } else {
    // all other cases
    returnValue = defaultTemperature;
  }

  return returnValue;
}



void Strategy::setUserCoordinates(byte userId, float lat, float lng, unsigned long unixTime) {
  const boolean LOCAL_DEBUG = false;

  if (userId <= numberOfUsers - 1) {
    if (LOCAL_DEBUG) {
      Serial.print("strategy.cpp: setUserCoordinates: userId: "); Serial.print(userId); Serial.print(", lat: "); Serial.print(lat, 10); Serial.print(", lng: "); Serial.print(lng, 10); Serial.print(", system time: "); Serial.println(unixTime);
    }
    userCoordinates[userId].lat = lat;
    userCoordinates[userId].lng = lng;
    userCoordinates[userId].uploadTime = unixTime;
  }

}


Strategy::LatLng Strategy::getUserCoordinates(byte userId) {
  return userCoordinates[userId];
}



unsigned int Strategy::getMinDistance(unsigned long unixTime) {

  const boolean LOCAL_DEBUG = false;
  const unsigned long EARTH_CIRCUMFERENCE = 40007834L; // average circumference (m) of the Earth

  unsigned int minDistance = 0; // in km
  boolean firstUser = true;

  for (byte i = 0; i <= numberOfUsers - 1; i++) {

    float userLat = userCoordinates[i].lat;
    float userLng = userCoordinates[i].lng;
    unsigned long userUploadTime = userCoordinates[i].uploadTime;

    if (unixTime - userUploadTime <= maxAge) {

      float dLat = userLat - latBesigheim;
      float dLng = userLng - lngBesigheim;

      float dx = dLng / 360 * EARTH_CIRCUMFERENCE * cos(latBesigheim * DEG_TO_RAD); // in m
      float dy = dLat / 360 * EARTH_CIRCUMFERENCE; // in m
      float ds = sqrt(pow(dx, 2) + pow(dy, 2)); // in m

      if (LOCAL_DEBUG) {
        Serial.print("Strategy::getMinDistance: userId: "); Serial.println(i);
        Serial.print("lat: "); Serial.print(userCoordinates[i].lat); Serial.print(", lng: "); Serial.println(userCoordinates[i].lng);
        Serial.print("dLat: "); Serial.print(dLat); Serial.print(", dLng: "); Serial.println(dLng);
        Serial.print("dx: "); Serial.print(dx); Serial.print(", dy: "); Serial.println(dy);
        Serial.print("ds: "); Serial.print(ds); Serial.print(", distance: "); Serial.println(round(ds / 1000.0));
      }

      if (firstUser) {
        minDistance = round(ds / 1000.0);
        firstUser = false;
      } else {
        minDistance = min(minDistance, round(ds / 1000.0));
      }
    }
  }

  return minDistance;
}

