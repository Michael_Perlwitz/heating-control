
#include <Arduino.h>
#include "valve.h"


Valve::Valve() {
  pinMode(valvePin, OUTPUT);
}


void Valve::setValvePosition(byte pos) {
  pos = constrain(pos, 0, 100);
  const float f = 0.7; // MOSFET steuert schon bei 70% des maximalen Wertes durch
  byte rawValue = (float) pos / 100 * 255 * f;
  analogWrite(valvePin, rawValue);
}


