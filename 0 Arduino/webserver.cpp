

#include <SPI.h>
#include <Ethernet2.h>

#include "webserver.h"
#include "webhelper.h"


static const boolean DEBUG = false;





void WebServer::setup() {
  server.begin();
}


void WebServer::serveClientRequests(WebServerParameter p) {

  char headerContent[256];
  char url[256];
  DecompUrl decompUrl;

  EthernetClient client = server.available(); // listen for incoming clients

  if (client) {
    boolean requestToBeServed = readRequestHeader(client, headerContent);
    if (requestToBeServed) {
      getUrlFromHeader(headerContent, url);
      decomposeUrl(url, &decompUrl);
      callActionHandler(client, p, &decompUrl);
    }
    delay(1000);
    client.stop();
  }

}




boolean WebServer::readRequestHeader(EthernetClient client, char *headerContent) {

  const boolean LOCAL_DEBUG = false;

  byte charCounter = 0;
  boolean newLine = false;
  boolean endOfRequest = false;

  while (client.connected() && !endOfRequest) {
    if (client.available()) {
      char c = client.read();
      if (LOCAL_DEBUG) Serial.println(c);

      if (charCounter < 255) { // limit analysis to 255 characters per line
        charCounter++;
        *(headerContent + charCounter - 1) = c; // array is zero-based
      }

      if (c != '\n' && c != '\r') {
        //if (LOCAL_DEBUG) Serial.println("newLine = false");
        newLine = false;
      } else if (c == '\n' && newLine) {
        if (LOCAL_DEBUG) Serial.println("endOfRequest = true");
        endOfRequest = true;
      } else if (c == '\n' && !newLine) {
        if (LOCAL_DEBUG) Serial.println("newLine = true");
        newLine = true;
      }
    }
  }

  boolean returnValue = charCounter > 0;

  if (LOCAL_DEBUG) {
    Serial.println("Header content: ");
    for (byte i = 0; i <= charCounter - 1; i++) Serial.print(*(headerContent + i));
    Serial.println();
    Serial.print("Return value: "); Serial.println(returnValue);
  }

  return returnValue;
}



void WebServer::getUrlFromHeader(char *headerContent, char *url) {

  const boolean LOCAL_DEBUG = false;

  // find position of GET
  byte offset = 0;
  for (byte i = 0; i <= 255 - 2; i++) {
    if ( *(headerContent + i) == 'G' && *(headerContent + i + 1) == 'E' && *(headerContent + i + 2) == 'T'  ) {
      offset = i + 5;
      break;
    }
  }
  if (LOCAL_DEBUG) {
    Serial.print("Offset: "); Serial.println(offset);
  }

  // read header until first blank
  for (byte i = 0; i <= 255 - offset - 1; i++) {
    if (*(headerContent + offset + i) != ' ') {
      *(url + i) = *(headerContent + offset + i);
    } else {
      *(url + i) = '\0';
      break;
    }
  }
  if (LOCAL_DEBUG) {
    Serial.print("Url: "); Serial.println(url);
  }

}



void WebServer::decomposeUrl(char *url, DecompUrl *decompUrl) {

  const boolean LOCAL_DEBUG = false;

  // invalidate all fields
  for (byte j = 0; j <= maxParams - 1; j++) {
    decompUrl->params[j].valid = false;
  }


  byte index = 0;

  // read file name
  for (byte i = 0; i <= maxFileNameLength - 1; i++) {
    if (*(url + i) != '?' && *(url + i) != '\0') {
      decompUrl->fileName[i] = *(url + i);
    } else {
      decompUrl->fileName[i] = '\0';
      index = i;
      break;
    }
  }
  if (LOCAL_DEBUG) {
    Serial.print("File name: ");
    for (byte i = 0; i <= maxFileNameLength - 1; i++) {
      if (decompUrl->fileName[i] != '\0')  {
        Serial.print(String(decompUrl->fileName[i]));
      } else {
        break;
      }
    }
    Serial.println();
  }

  // continue only, if next character is '?'
  if (*(url + index) != '?') {
    return;
  } else {
    index++;
  }



  for (byte j = 0; j <= maxParams - 1; j++) {

    boolean nameAvailable = false;
    boolean valueAvailable = false;

    // read parameter name

    for (byte i = 0; i <= maxParamNameLength - 1; i++) {
      if (*(url + index + i) != '=' && *(url + index + i) != '\0') {
        decompUrl->params[j].name[i] = *(url + index + i);
        nameAvailable = true;
      } else {
        decompUrl->params[j].name[i] = '\0';
        index += i;
        break;
      }
    }
    if (LOCAL_DEBUG) {
      Serial.print("Parameter name: ");
      for (byte i = 0; i <= maxParamNameLength - 1; i++) {
        if (decompUrl->params[j].name[i] != '\0')  {
          Serial.print(decompUrl->params[j].name[i]);
        } else {
          break;
        }
      }
      Serial.println();
    }

    // continue only, if next character is '='
    if (*(url + index) != '=') {
      return;
    } else {
      index++;
    }


    // read parameter value

    for (byte i = 0; i <= maxParamValueLength - 1; i++) {
      if (*(url + index + i) != '&' && *(url + index + i) != '\0') {
        //Serial.println(String("anhängen: ") + String(i));
        decompUrl->params[j].value[i] = *(url + index + i);
        valueAvailable = true;
      } else {
        //Serial.println(String("Ende: ") + String(i));
        decompUrl->params[j].value[i] = '\0';
        index += i;
        break;
      }
    }
    if (LOCAL_DEBUG) {
      Serial.print("Parameter value: ");
      for (byte i = 0; i <= maxParamValueLength - 1; i++) {
        if (decompUrl->params[j].value[i] != '\0')  {
          Serial.print(decompUrl->params[j].value[i]);
        } else {
          break;
        }
      }
      Serial.println();
    }

    // if there is name and value, make struct item valid

    decompUrl->params[j].valid = nameAvailable && valueAvailable;
    if (LOCAL_DEBUG) {
      Serial.print("Parameter valid: "); Serial.println(decompUrl->params[j].valid);
    }

    // continue only, if next character is '&'
    if (*(url + index) != '&') {
      return;
    } else {
      index++;
    }

  }

}




void WebServer::callActionHandler(EthernetClient client, WebServer::WebServerParameter p, DecompUrl *decompUrl) {

  const boolean LOCAL_DEBUG = false;


  if (strncmp(decompUrl->params[1].value, credential, 3) == 0 && decompUrl->params[1].valid) { // check credential

    if (strncmp(decompUrl->params[0].name, "TempStrategy", 12) == 0 && decompUrl->params[0].valid) {
      if (strncmp(decompUrl->params[0].value, "off", 3) == 0) {
        (p.pStrategy)->setMode(0, 0);
      } else if (strncmp(decompUrl->params[0].value, "automatic", 9) == 0) {
        (p.pStrategy)->setMode(1, 0);
      } else if (strncmp(decompUrl->params[0].value, "19", 2) == 0) {
        (p.pStrategy)->setMode(2, 19);
      } else if (strncmp(decompUrl->params[0].value, "20", 2) == 0) {
        (p.pStrategy)->setMode(2, 20);
      } else if (strncmp(decompUrl->params[0].value, "21", 2) == 0) {
        (p.pStrategy)->setMode(2, 21);
      } else if (strncmp(decompUrl->params[0].value, "22", 2) == 0) {
        (p.pStrategy)->setMode(2, 22);
      } else if (strncmp(decompUrl->params[0].value, "23", 2) == 0) {
        (p.pStrategy)->setMode(2, 23);
      } else if (strncmp(decompUrl->params[0].value, "max", 3) == 0) {
        (p.pStrategy)->setMode(9, 0);
      } else {
        (p.pStrategy)->setMode(1, 0);
      }
    }
  }


  if (strncmp(decompUrl->params[12].value, credential, 3) == 0 && decompUrl->params[12].valid) { // check credential

    Eprom eprom;
    Eprom::Temperatures newTemp;

    if (strncmp(decompUrl->params[0].name, "fromHour0", 9) == 0 && decompUrl->params[0].valid) {
      newTemp.item[0].fromHour = (byte) atoi(decompUrl->params[0].value);
    }
    if (strncmp(decompUrl->params[1].name, "toHour0", 7) == 0 && decompUrl->params[1].valid) {
      newTemp.item[0].toHour = (byte) atoi(decompUrl->params[1].value);
    }
    if (strncmp(decompUrl->params[2].name, "temp0", 5) == 0 && decompUrl->params[2].valid) {
      newTemp.item[0].temp = (byte) atoi(decompUrl->params[2].value);
    }

    if (strncmp(decompUrl->params[3].name, "fromHour1", 9) == 0 && decompUrl->params[3].valid) {
      newTemp.item[1].fromHour = (byte) atoi(decompUrl->params[3].value);
    }
    if (strncmp(decompUrl->params[4].name, "toHour1", 7) == 0 && decompUrl->params[4].valid) {
      newTemp.item[1].toHour = (byte) atoi(decompUrl->params[4].value);
    }
    if (strncmp(decompUrl->params[5].name, "temp1", 5) == 0 && decompUrl->params[5].valid) {
      newTemp.item[1].temp = (byte) atoi(decompUrl->params[5].value);
    }

    if (strncmp(decompUrl->params[6].name, "fromHour2", 9) == 0 && decompUrl->params[6].valid) {
      newTemp.item[2].fromHour = (byte) atoi(decompUrl->params[6].value);
    }
    if (strncmp(decompUrl->params[7].name, "toHour2", 7) == 0 && decompUrl->params[7].valid) {
      newTemp.item[2].toHour = (byte) atoi(decompUrl->params[7].value);
    }
    if (strncmp(decompUrl->params[8].name, "temp2", 5) == 0 && decompUrl->params[8].valid) {
      newTemp.item[2].temp = (byte) atoi(decompUrl->params[8].value);
    }

    if (strncmp(decompUrl->params[9].name, "fromHour3", 9) == 0 && decompUrl->params[9].valid) {
      newTemp.item[3].fromHour = (byte) atoi(decompUrl->params[9].value);
    }
    if (strncmp(decompUrl->params[10].name, "toHour3", 7) == 0 && decompUrl->params[10].valid) {
      newTemp.item[3].toHour = (byte) atoi(decompUrl->params[10].value);
    }
    if (strncmp(decompUrl->params[11].name, "temp3", 5) == 0 && decompUrl->params[11].valid) {
      newTemp.item[3].temp = (byte) atoi(decompUrl->params[11].value);
    }
    Serial.println("call setSchedule");
    eprom.setSchedule(newTemp);
  }


  if (strncmp(decompUrl->params[3].value, credential, 3) == 0 && decompUrl->params[3].valid) { // check credential

    byte userId = 255;
    float lat = -1.0f;
    float lng = -1.0f;

    if (strncmp(decompUrl->params[0].name, "userID", 6) == 0 && decompUrl->params[0].valid) {
      // derive userId from IMSI
      if (strncmp(decompUrl->params[0].value, "262021905024238", 15) == 0) {
        userId = Strategy::Michael;
      } else if (strncmp(decompUrl->params[0].value, "262021704069591", 15) == 0) {
        userId = Strategy::Luisa;
      };
    }
    if (strncmp(decompUrl->params[1].name, "lat", 3) == 0 && decompUrl->params[1].valid) {
      lat = atof(decompUrl->params[1].value);
    }
    if (strncmp(decompUrl->params[2].name, "lng", 3) == 0 && decompUrl->params[2].valid) {
      lng = atof(decompUrl->params[2].value);
    }

    if (LOCAL_DEBUG) {
      Serial.print("webServer.cpp: setUserCoordinates: userId: "); Serial.print(userId); Serial.print(", lat: "); Serial.print(lat); Serial.print(", lng: "); Serial.print(lng); Serial.print(", system time: "); Serial.println(p.unixTime);
    }

    if (userId != 255 && lat != -1.0f && lng != -1.0f) {
      (p.pStrategy)->setUserCoordinates(userId, lat, lng, p.unixTime);
    }
  }


  sendHttpResponseHeader(client);
  sendHtmlBody(client, p, decompUrl->fileName);

}




void WebServer::sendHttpResponseHeader(EthernetClient client) {

  client.println(F("HTTP/1.1 200 OK"));
  client.println(F("Content-Type: text/html"));
  client.println(F("Connection: close"));  // the connection will be closed after completion of the response
  client.println();

}



void WebServer::sendHtmlBody(EthernetClient client, WebServer::WebServerParameter p, char url[maxFileNameLength]) {

  const boolean LOCAL_DEBUG = false;

  if (LOCAL_DEBUG) {
    Serial.print("url: "); Serial.println(url);
  }


  // if empty filename given, resort to index.htm
  if (url[0] == '\0') {
    url = (char *) "index.htm";
  }

  if (strncmp(url, "index.htm", 9) == 0 || strncmp(url, "control.htm", 11) == 0 || strncmp(url, "schedule.htm", 12) == 0 || strncmp(url, "map.htm", 7) == 0 || strncmp(url, "system.htm", 7) == 0 || strncmp(url, "upload.htm", 7) == 0) {

    if (LOCAL_DEBUG) Serial.println("use streamfile");
    StreamFile::ReplacementItems r;
    WebHelper::prepareReplacementItems(&r, p);
    StreamFile sf;
    sf.setup(url, r);
    while (sf.hasNext()) {
      char c = sf.readChar();
      if (LOCAL_DEBUG) Serial.print(c);
      client.print(c);
    }
    sf.closeFile();



  } else if (strncmp(url, "stat.htm", 7) == 0) {

    const int numberOfLogLines = 3 * 24 * 60 / 5; // 3 * 24 hours with measurements of every 5 minutes

    if (LOCAL_DEBUG) Serial.println("combine html with reporting log");
    {
      Repo r;
      r.setup((char *) "stat.htm", NULL, (char *) "[34]");
      while (r.hasNext()) {
        char c = r.readChar();
        if (LOCAL_DEBUG) Serial.println(c);
        client.print(c);
      }
      r.closeFile();
    }
    {
      Repo r;
      File f = r.setupReadLog();
      boolean first = true;

      if (r.MovePointerToNLastLogEntries(f, numberOfLogLines)) {
        while (!r.endOfLog(f)) {
          char reportLine[30];
          boolean valid = r.getFormattedLogLine(f, Repo::actualTempReport, reportLine);
               
          if (valid) {
            if (first) {
              if (LOCAL_DEBUG) Serial.print(reportLine);
              client.print(reportLine);
              first = false;
            } else {
              if (LOCAL_DEBUG) {
                Serial.print(",\n");
                Serial.print(reportLine);
              }
              client.print(",\n");
              client.print(reportLine);
            }
          }

        }
        r.closeLog(f);
      }
    }
    {
      Repo r;
      r.setup((char *) "stat.htm", (char *) "[34]", (char *) "[35]");
      while (r.hasNext()) {
        char c = r.readChar();
        if (LOCAL_DEBUG) Serial.println(c);
        client.print(c);
      }
      r.closeFile();
    }
    {
      Repo r;
      File f = r.setupReadLog();
      boolean first = true;

      if (r.MovePointerToNLastLogEntries(f, numberOfLogLines)) {
        while (!r.endOfLog(f)) {
          char reportLine[30];
          boolean valid = r.getFormattedLogLine(f, Repo::targetTempReport, reportLine);

          if (valid) {
            if (first) {
              if (LOCAL_DEBUG) Serial.print(reportLine);
              client.print(reportLine);
              first = false;
            } else {
              if (LOCAL_DEBUG) {
                Serial.print(",\n");
                Serial.print(reportLine);
              }
              client.print(",\n");
              client.print(reportLine);
            }
          }

        }
        r.closeLog(f);
      }
    }
    {
      Repo r;
      r.setup((char *) "stat.htm", (char *) "[35]", (char *) "[36]");
      while (r.hasNext()) {
        char c = r.readChar();
        if (LOCAL_DEBUG) Serial.println(c);
        client.print(c);
      }
      r.closeFile();
    }
    {
      Repo r;
      File f = r.setupReadLog();
      boolean first = true;

      if (r.MovePointerToNLastLogEntries(f, numberOfLogLines)) {
        while (!r.endOfLog(f)) {
          char reportLine[30];
          boolean valid = r.getFormattedLogLine(f, Repo::positionReport, reportLine);

          if (valid) {
            if (first) {
              if (LOCAL_DEBUG) Serial.print(reportLine);
              client.print(reportLine);
              first = false;
            } else {
              if (LOCAL_DEBUG) {
                Serial.print(",\n");
                Serial.print(reportLine);
              }
              client.print(",\n");
              client.print(reportLine);
            }
          }

        }
        r.closeLog(f);
      }
    }
    {
      Repo r;
      r.setup((char *) "stat.htm", (char *) "[36]", NULL);
      while (r.hasNext()) {
        char c = r.readChar();
        if (LOCAL_DEBUG) Serial.println(c);
        client.print(c);
      }
      r.closeFile();
    }
  }

}
