
#ifndef STRATEGY_H
#define STRATEGY_H

#include "strategy.h"
#include "eprom.h"


class Strategy {



  public:

    enum { off = 0, automatic = 1, fixedTemp = 2, max = 9 };
    enum { Michael = 0, Luisa = 1 };
    const byte minTemp = 5;    
    const byte defaultTemperature = 20;
 
    typedef struct {
      float lat;
      float lng;
      unsigned long uploadTime;
    } LatLng;

    Strategy();

    void setMode(byte mode, byte fixedTargetTemp);
    byte getMode();
    byte getFixedTargetTemp();

    static const unsigned long maxAge = 20*60*1000L; // milli seconds
    const float tempCorrPerKm = 3.0/20.0; // distance-related temperature correction in degrees per km
    void setUserCoordinates(byte userId, float lat, float lng, unsigned long unixTime);
    LatLng getUserCoordinates(byte userId);

    byte getTemperatureTarget(float h, unsigned long unixTime, Eprom::Temperatures temp);


  private:

    byte mode;
    byte fixedTargetTemp;   

    static const byte numberOfUsers = 2;
    LatLng userCoordinates[numberOfUsers];
    unsigned int getMinDistance(unsigned long unixTime);

    const float latBesigheim = 48.995051;
    const float lngBesigheim = 9.125656;

};

#endif
