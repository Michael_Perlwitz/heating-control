
#include <EEPROM.h>
#include "eprom.h"

static const boolean DEBUG = false;

static const byte addrMagicByte0 = 0;
static const byte addrMagicByte1 = 1;
static const byte addrMagicByte2 = 2;

static const byte addrFromHour0 = 3;
static const byte addrToHour0 = 4;
static const byte addrTemp0 = 5;

static const byte addrFromHour1 = 6;
static const byte addrToHour1 = 7;
static const byte addrTemp1 = 8;

static const byte addrFromHour2 = 9;
static const byte addrToHour2 = 10;
static const byte addrTemp2 = 11;

static const byte addrFromHour3 = 12;
static const byte addrToHour3 = 13;
static const byte addrTemp3 = 14;

static const byte addrBootCounter = 15;




void Eprom::setMagicByte() {
  EEPROM.write(addrMagicByte0, 18);
  EEPROM.write(addrMagicByte1, 12);
  EEPROM.write(addrMagicByte2, 74);
}

boolean Eprom::isMagicByteSet() {
  return (EEPROM.read(addrMagicByte0) == 18 && EEPROM.read(addrMagicByte1) == 12 && EEPROM.read(addrMagicByte2) == 74);
}



Eprom::Eprom() {

  // check if EEPROM contains valid temperature schedule

  if (!isMagicByteSet()) {

    EEPROM.write(addrFromHour0, 0);
    EEPROM.write(addrToHour0, 6);
    EEPROM.write(addrTemp0, 18);

    EEPROM.write(addrFromHour1, 6);
    EEPROM.write(addrToHour1, 20);
    EEPROM.write(addrTemp1, 23);

    EEPROM.write(addrFromHour2, 20);
    EEPROM.write(addrToHour2, 23);
    EEPROM.write(addrTemp2, 21);

    EEPROM.write(addrFromHour3, 23);
    EEPROM.write(addrToHour3, 24);
    EEPROM.write(addrTemp3, 18);

    EEPROM.write(addrBootCounter, 0);

    setMagicByte();

  }
}



Eprom::Temperatures Eprom::getSchedule() {

  Temperatures temp;

  if (DEBUG) {
    Serial.print("Eprom::getSchedule "); Serial.println(EEPROM.read(addrFromHour0));
  }

  temp.item[0].fromHour = EEPROM.read(addrFromHour0);
  temp.item[0].toHour = EEPROM.read(addrToHour0);
  temp.item[0].temp = EEPROM.read(addrTemp0);

  temp.item[1].fromHour = EEPROM.read(addrFromHour1);
  temp.item[1].toHour = EEPROM.read(addrToHour1);
  temp.item[1].temp = EEPROM.read(addrTemp1);

  temp.item[2].fromHour = EEPROM.read(addrFromHour2);
  temp.item[2].toHour = EEPROM.read(addrToHour2);
  temp.item[2].temp = EEPROM.read(addrTemp2);

  temp.item[3].fromHour = EEPROM.read(addrFromHour3);
  temp.item[3].toHour = EEPROM.read(addrToHour3);
  temp.item[3].temp = EEPROM.read(addrTemp3);

  return temp;
}



void Eprom::setSchedule(Temperatures newTemp) {

  if (DEBUG) {
    Serial.print("Eprom::setSchedule "); Serial.println(newTemp.item[0].fromHour);
  }

  EEPROM.write(addrFromHour0, newTemp.item[0].fromHour);
  EEPROM.write(addrToHour0, newTemp.item[0].toHour);
  EEPROM.write(addrTemp0, newTemp.item[0].temp);

  EEPROM.write(addrFromHour1, newTemp.item[1].fromHour);
  EEPROM.write(addrToHour1, newTemp.item[1].toHour);
  EEPROM.write(addrTemp1, newTemp.item[1].temp);

  EEPROM.write(addrFromHour2, newTemp.item[2].fromHour);
  EEPROM.write(addrToHour2, newTemp.item[2].toHour);
  EEPROM.write(addrTemp2, newTemp.item[2].temp);

  EEPROM.write(addrFromHour3, newTemp.item[3].fromHour);
  EEPROM.write(addrToHour3, newTemp.item[3].toHour);
  EEPROM.write(addrTemp3, newTemp.item[3].temp);

}



int Eprom::getBootCounter() {

  byte bootCounter;
  if (isMagicByteSet()) {
    bootCounter = EEPROM.read(addrBootCounter);
  } else {
    bootCounter = 0;
  }

  return bootCounter;
}



void Eprom::incrementBootCounter() {

  byte bootCounter;

  if (isMagicByteSet()) {
    bootCounter = EEPROM.read(addrBootCounter);
  } else {
    bootCounter = 0;
  }

  bootCounter++;
  EEPROM.write(addrBootCounter, bootCounter);

}




