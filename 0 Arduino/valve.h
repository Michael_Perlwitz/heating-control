
#ifndef VALVE_H
#define VALVE_H

#include "valve.h"


class Valve {

  private:

    const byte valvePin = 9;


  public:

    Valve();
    void setValvePosition(byte pos);

};


#endif

