
#ifndef EPROM_H
#define EPROM_H

#include <Arduino.h>
#include "repo.h"


class Eprom {


  public:

    static const byte tempMaxItems = 4; // 4 means 0...3 as array index!

    typedef struct {

      typedef struct {
        byte fromHour = 0;
        byte toHour = 0;
        byte temp = 0;
      } TempItem;

      TempItem item[tempMaxItems];

    } Temperatures;


    Eprom();

    Temperatures getSchedule();
    void setSchedule(Temperatures newTemp);

    int getBootCounter();
    void incrementBootCounter();



  private:

    void setMagicByte();
    boolean isMagicByteSet();


};

#endif

