package net.perlwitz.michael.heatingcontrol;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;
import static net.perlwitz.michael.heatingcontrol.MainActivity.setAlarm;


public class AlarmReceiver extends BroadcastReceiver {


    private static final boolean DEBUG = true;
    private static String subscriberId = null;
    private static final OkHttpClient okHttpClient = new OkHttpClient();


    @SuppressLint("HardwareIds")
    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(Globals.TAG, "AlarmReceiver:onReceive: method entry");

        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        if (telephonyManager != null) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                subscriberId = telephonyManager.getSubscriberId();
                if (DEBUG) Log.d(Globals.TAG, "subscriberId IMSI: " + subscriberId);
            }
        }

        getLastLocation(context);

        setAlarm(context);
    }


    private void getLastLocation(Context context) {

        FusedLocationProviderClient locationClient = getFusedLocationProviderClient(context);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                onLocationChanged(location);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            if (DEBUG)
                                Log.d("MapDemoActivity", "Error trying to get last GPS location");
                            e.printStackTrace();
                        }
                    });
        }
    }


    private void onLocationChanged(Location location) {

        if (DEBUG) Log.d(Globals.TAG, "onLocationChanged: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude()));

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        okHttpCall(latLng);

    }


    private void okHttpCall(final LatLng latLng) {

        if (DEBUG) Log.d(Globals.TAG, "OkHttpCall:okHttpCall: method entry");

        NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
        DecimalFormat df = (DecimalFormat) nf;
        df.applyPattern("#.######");

        String strLat = df.format(latLng.latitude);
        String strLng = df.format(latLng.longitude);

        Request.Builder requestBuilder = new Request.Builder();
        Request request;

        HttpUrl.Builder urlBuilder = HttpUrl.parse(Globals.serverUrl).newBuilder();

        urlBuilder
                .addQueryParameter("userID", subscriberId)
                .addQueryParameter("lat", strLat)
                .addQueryParameter("lng", strLng)
                .addQueryParameter("passwd", Globals.serverPassword);
        String url = urlBuilder.build().toString();

        request = requestBuilder.url(url).build();


        okHttpClient.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Request request, IOException e) {
                if (DEBUG) {
                    Log.d(Globals.TAG, "OkHttpCall:okHttpCall:Callback:onFailure: method entry");
                }
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (DEBUG) {
                    Log.d(Globals.TAG, "OkHttpCall:okHttpCall:Callback:onResponse: method entry");
                    Log.d(Globals.TAG, response.body().string());
                }
            }


        });


    }
}