package net.perlwitz.michael.heatingcontrol;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import static net.perlwitz.michael.heatingcontrol.MainActivity.setAlarm;




public class BootReceiver extends BroadcastReceiver {


    private static final boolean DEBUG = true;


    @Override
    public void onReceive(Context context, Intent intent) {
        if (DEBUG) Log.d(Globals.TAG, "BootReceiver:onReceive: method entry");

        //noinspection ConstantConditions
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            setAlarm(context);
        }

    }
}


