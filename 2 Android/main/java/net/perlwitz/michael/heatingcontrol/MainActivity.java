package net.perlwitz.michael.heatingcontrol;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;



public class MainActivity extends AppCompatActivity {

    private static final boolean DEBUG = true;

    private static final int LOCATION_PERMISSIONS_REQUEST = 1;
    private static final int PHONE_PERMISSION_REQUEST = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (DEBUG) Log.d(Globals.TAG, "MainActivity:onCreate started");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (DEBUG) Log.d(Globals.TAG, "MainActivity:onCreate check permissions");
        getPermissions();

        setAlarm(getApplicationContext());

    }


    public static void setAlarm(Context context) {
        if (DEBUG) Log.d(Globals.TAG, "MainActivity:setAlarm");

        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);


        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);

            alarmManager.setAndAllowWhileIdle(
                    AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + Globals.alarmInterval,
                    pendingIntent);

            if (DEBUG) Log.d(Globals.TAG, "MainActivity:setAlarm successfully set");
        }

    }





    private void getPermissions() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSIONS_REQUEST);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSIONS_REQUEST);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, PHONE_PERMISSION_REQUEST);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        if (requestCode == LOCATION_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (DEBUG) Log.d(Globals.TAG, "Location permission granted");
            } else {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
        if (requestCode == PHONE_PERMISSION_REQUEST) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (DEBUG) Log.d(Globals.TAG, "Phone read permission granted");
            } else {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }


    }
}